<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* detail_match.html.twig */
class __TwigTemplate_ca78c65a0dae370a767758e996e241632ec542c24c51eef9b7821398fd3dfe34 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "detail_match.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "detail_match.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "detail_match.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 2
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 3
        echo "    <br>
    <div class=\"container col-md-10\" >
        <table class=\"table-sm table-bordered table-hover\" align =\"center\" style=\"text-align: center;background-color: #8395a7;\">
            <tr>
                <th>feature</th>
                <th>Kills</th>
                <th>Minions</th>
                <th>goldEarned</th>
                <th>first_blood</th>
                <th>first_tower</th>
                <th>first_inhibitor</th>
                <th>first_baron</th>
                <th>first_dragon</th>
                <th>first_RiftHerald</th>
                <th>t1_tower</th>
                <th>t1_inhibitor</th>
                <th>t1_baron</th>
                <th>t1_dragon</th>
                <th>Issue</th>
            </tr>
            ";
        // line 23
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["teams"]) || array_key_exists("teams", $context) ? $context["teams"] : (function () { throw new RuntimeError('Variable "teams" does not exist.', 23, $this->source); })()));
        $context['loop'] = [
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        ];
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["team"]) {
            // line 24
            echo "                <tr>
                    <th>value team";
            // line 25
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["loop"], "index", [], "any", false, false, false, 25), "html", null, true);
            echo "</th>
                    <td>
                        ";
            // line 27
            if ((0 === twig_compare(twig_get_attribute($this->env, $this->source, $context["team"], "teamId", [], "any", false, false, false, 27), 100))) {
                // line 28
                echo "                            ";
                echo twig_escape_filter($this->env, (isset($context["killsParticipantsTeam1"]) || array_key_exists("killsParticipantsTeam1", $context) ? $context["killsParticipantsTeam1"] : (function () { throw new RuntimeError('Variable "killsParticipantsTeam1" does not exist.', 28, $this->source); })()), "html", null, true);
                echo "
                        ";
            } else {
                // line 30
                echo "                            ";
                echo twig_escape_filter($this->env, (isset($context["killsParticipantsTeam2"]) || array_key_exists("killsParticipantsTeam2", $context) ? $context["killsParticipantsTeam2"] : (function () { throw new RuntimeError('Variable "killsParticipantsTeam2" does not exist.', 30, $this->source); })()), "html", null, true);
                echo "
                        ";
            }
            // line 32
            echo "                    </td>
                    <td>
                        ";
            // line 34
            if ((0 === twig_compare(twig_get_attribute($this->env, $this->source, $context["team"], "teamId", [], "any", false, false, false, 34), 100))) {
                // line 35
                echo "                            ";
                echo twig_escape_filter($this->env, (isset($context["totalMinionsKilledParticipantsTeam1"]) || array_key_exists("totalMinionsKilledParticipantsTeam1", $context) ? $context["totalMinionsKilledParticipantsTeam1"] : (function () { throw new RuntimeError('Variable "totalMinionsKilledParticipantsTeam1" does not exist.', 35, $this->source); })()), "html", null, true);
                echo "
                        ";
            } else {
                // line 37
                echo "                            ";
                echo twig_escape_filter($this->env, (isset($context["totalMinionsKilledParticipantsTeam2"]) || array_key_exists("totalMinionsKilledParticipantsTeam2", $context) ? $context["totalMinionsKilledParticipantsTeam2"] : (function () { throw new RuntimeError('Variable "totalMinionsKilledParticipantsTeam2" does not exist.', 37, $this->source); })()), "html", null, true);
                echo "
                        ";
            }
            // line 39
            echo "                    </td>
                    <td>
                        ";
            // line 41
            if ((0 === twig_compare(twig_get_attribute($this->env, $this->source, $context["team"], "teamId", [], "any", false, false, false, 41), 100))) {
                // line 42
                echo "                            ";
                echo twig_escape_filter($this->env, (isset($context["goldEarnedParticipantsTeam1"]) || array_key_exists("goldEarnedParticipantsTeam1", $context) ? $context["goldEarnedParticipantsTeam1"] : (function () { throw new RuntimeError('Variable "goldEarnedParticipantsTeam1" does not exist.', 42, $this->source); })()), "html", null, true);
                echo "
                        ";
            } else {
                // line 44
                echo "                            ";
                echo twig_escape_filter($this->env, (isset($context["goldEarnedParticipantsTeam2"]) || array_key_exists("goldEarnedParticipantsTeam2", $context) ? $context["goldEarnedParticipantsTeam2"] : (function () { throw new RuntimeError('Variable "goldEarnedParticipantsTeam2" does not exist.', 44, $this->source); })()), "html", null, true);
                echo "
                        ";
            }
            // line 46
            echo "                    </td>
                    <td>
                        ";
            // line 48
            if ((0 === twig_compare(twig_get_attribute($this->env, $this->source, $context["team"], "firstBlood", [], "any", false, false, false, 48), false))) {
                // line 49
                echo "                            0
                        ";
            } else {
                // line 51
                echo "                            ";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["team"], "firstBlood", [], "any", false, false, false, 51), "html", null, true);
                echo "
                        ";
            }
            // line 53
            echo "                    </td>
                    <td>
                        ";
            // line 55
            if ((0 === twig_compare(twig_get_attribute($this->env, $this->source, $context["team"], "firstTower", [], "any", false, false, false, 55), false))) {
                // line 56
                echo "                            0
                        ";
            } else {
                // line 58
                echo "                            ";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["team"], "firstTower", [], "any", false, false, false, 58), "html", null, true);
                echo "
                        ";
            }
            // line 60
            echo "                    </td>
                    <td>
                        ";
            // line 62
            if ((0 === twig_compare(twig_get_attribute($this->env, $this->source, $context["team"], "firstInhibitor", [], "any", false, false, false, 62), false))) {
                // line 63
                echo "                            0
                        ";
            } else {
                // line 65
                echo "                            ";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["team"], "firstInhibitor", [], "any", false, false, false, 65), "html", null, true);
                echo "
                        ";
            }
            // line 67
            echo "                    </td>
                    <td>
                        ";
            // line 69
            if ((0 === twig_compare(twig_get_attribute($this->env, $this->source, $context["team"], "firstBaron", [], "any", false, false, false, 69), false))) {
                // line 70
                echo "                            0
                        ";
            } else {
                // line 72
                echo "                            ";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["team"], "firstBaron", [], "any", false, false, false, 72), "html", null, true);
                echo "
                        ";
            }
            // line 74
            echo "                    </td>
                    <td>
                        ";
            // line 76
            if ((0 === twig_compare(twig_get_attribute($this->env, $this->source, $context["team"], "firstDragon", [], "any", false, false, false, 76), false))) {
                // line 77
                echo "                            0
                        ";
            } else {
                // line 79
                echo "                            ";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["team"], "firstDragon", [], "any", false, false, false, 79), "html", null, true);
                echo "
                        ";
            }
            // line 81
            echo "                    </td>
                    <td>
                        ";
            // line 83
            if ((0 === twig_compare(twig_get_attribute($this->env, $this->source, $context["team"], "firstRiftHerald", [], "any", false, false, false, 83), false))) {
                // line 84
                echo "                            0
                        ";
            } else {
                // line 86
                echo "                            ";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["team"], "firstRiftHerald", [], "any", false, false, false, 86), "html", null, true);
                echo "
                        ";
            }
            // line 88
            echo "                    </td>
                    <td>";
            // line 89
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["team"], "towerKills", [], "any", false, false, false, 89), "html", null, true);
            echo "</td>
                    <td>";
            // line 90
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["team"], "inhibitorKills", [], "any", false, false, false, 90), "html", null, true);
            echo "</td>
                    <td>";
            // line 91
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["team"], "baronKills", [], "any", false, false, false, 91), "html", null, true);
            echo "</td>
                    <td>";
            // line 92
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["team"], "dragonKills", [], "any", false, false, false, 92), "html", null, true);
            echo "</td>
                    <td>
                        ";
            // line 94
            if ((0 === twig_compare(twig_get_attribute($this->env, $this->source, $context["team"], "win", [], "any", false, false, false, 94), "Win"))) {
                // line 95
                echo "                            Win
                        ";
            } else {
                // line 97
                echo "                            Loose
                        ";
            }
            // line 99
            echo "                    </td>
                </tr>

            ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['team'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 103
        echo "
        </table>
        <div class=\"row\" style=\"margin: 10px;\">

            ";
        // line 107
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["participants"]) || array_key_exists("participants", $context) ? $context["participants"] : (function () { throw new RuntimeError('Variable "participants" does not exist.', 107, $this->source); })()));
        $context['loop'] = [
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        ];
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["participant"]) {
            // line 108
            echo "                ";
            if ((0 >= twig_compare(twig_get_attribute($this->env, $this->source, $context["loop"], "index", [], "any", false, false, false, 108), 5))) {
                // line 109
                echo "                    <div class=\"card\" style=\"width: 18rem;margin-right: 15px;\">
                        <div class=\"card-header\" style=\"background-color: #34495e\">
                            Equipe 1
                        </div>
                        <ul class=\"list-group list-group-flush\">
                            ";
                // line 114
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable((isset($context["participantsIdentities"]) || array_key_exists("participantsIdentities", $context) ? $context["participantsIdentities"] : (function () { throw new RuntimeError('Variable "participantsIdentities" does not exist.', 114, $this->source); })()));
                foreach ($context['_seq'] as $context["_key"] => $context["participantsIdentitie"]) {
                    // line 115
                    echo "                                ";
                    if ((0 === twig_compare(twig_get_attribute($this->env, $this->source, $context["participant"], "participantId", [], "any", false, false, false, 115), twig_get_attribute($this->env, $this->source, $context["participantsIdentitie"], "participantId", [], "any", false, false, false, 115)))) {
                        // line 116
                        echo "                                      <li class=\"list-group-item\">";
                        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["participantsIdentitie"], "player", [], "any", false, false, false, 116), "summonerName", [], "any", false, false, false, 116), "html", null, true);
                        echo "</li>
                                ";
                    }
                    // line 118
                    echo "                            ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['participantsIdentitie'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 119
                echo "                        </ul>
                    </div>
                ";
            }
            // line 122
            echo "            ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['participant'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 123
        echo "
        </div>
        <br>
        <div class=\"row\" style=\"margin-left: 10px;\">

            ";
        // line 128
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["participants"]) || array_key_exists("participants", $context) ? $context["participants"] : (function () { throw new RuntimeError('Variable "participants" does not exist.', 128, $this->source); })()));
        $context['loop'] = [
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        ];
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["participant"]) {
            // line 129
            echo "                ";
            if ((1 === twig_compare(twig_get_attribute($this->env, $this->source, $context["loop"], "index", [], "any", false, false, false, 129), 5))) {
                // line 130
                echo "
                    <div class=\"card\" style=\"width: 18rem;margin-right: 15px;\">
                        <div class=\"card-header\" style=\"background-color: #c0392b\">
                            Equipe 2
                        </div>
                        <ul class=\"list-group list-group-flush\">
                            ";
                // line 136
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable((isset($context["participantsIdentities"]) || array_key_exists("participantsIdentities", $context) ? $context["participantsIdentities"] : (function () { throw new RuntimeError('Variable "participantsIdentities" does not exist.', 136, $this->source); })()));
                foreach ($context['_seq'] as $context["_key"] => $context["participantsIdentitie"]) {
                    // line 137
                    echo "                                ";
                    if ((0 === twig_compare(twig_get_attribute($this->env, $this->source, $context["participant"], "participantId", [], "any", false, false, false, 137), twig_get_attribute($this->env, $this->source, $context["participantsIdentitie"], "participantId", [], "any", false, false, false, 137)))) {
                        // line 138
                        echo "                                    <li class=\"list-group-item\">";
                        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["participantsIdentitie"], "player", [], "any", false, false, false, 138), "summonerName", [], "any", false, false, false, 138), "html", null, true);
                        echo "</li>
                                ";
                    }
                    // line 140
                    echo "                            ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['participantsIdentitie'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 141
                echo "                        </ul>
                    </div>
                ";
            }
            // line 144
            echo "            ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['participant'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 145
        echo "        </div>
    </div>

";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "detail_match.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  452 => 145,  438 => 144,  433 => 141,  427 => 140,  421 => 138,  418 => 137,  414 => 136,  406 => 130,  403 => 129,  386 => 128,  379 => 123,  365 => 122,  360 => 119,  354 => 118,  348 => 116,  345 => 115,  341 => 114,  334 => 109,  331 => 108,  314 => 107,  308 => 103,  291 => 99,  287 => 97,  283 => 95,  281 => 94,  276 => 92,  272 => 91,  268 => 90,  264 => 89,  261 => 88,  255 => 86,  251 => 84,  249 => 83,  245 => 81,  239 => 79,  235 => 77,  233 => 76,  229 => 74,  223 => 72,  219 => 70,  217 => 69,  213 => 67,  207 => 65,  203 => 63,  201 => 62,  197 => 60,  191 => 58,  187 => 56,  185 => 55,  181 => 53,  175 => 51,  171 => 49,  169 => 48,  165 => 46,  159 => 44,  153 => 42,  151 => 41,  147 => 39,  141 => 37,  135 => 35,  133 => 34,  129 => 32,  123 => 30,  117 => 28,  115 => 27,  110 => 25,  107 => 24,  90 => 23,  68 => 3,  58 => 2,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}
{% block body %}
    <br>
    <div class=\"container col-md-10\" >
        <table class=\"table-sm table-bordered table-hover\" align =\"center\" style=\"text-align: center;background-color: #8395a7;\">
            <tr>
                <th>feature</th>
                <th>Kills</th>
                <th>Minions</th>
                <th>goldEarned</th>
                <th>first_blood</th>
                <th>first_tower</th>
                <th>first_inhibitor</th>
                <th>first_baron</th>
                <th>first_dragon</th>
                <th>first_RiftHerald</th>
                <th>t1_tower</th>
                <th>t1_inhibitor</th>
                <th>t1_baron</th>
                <th>t1_dragon</th>
                <th>Issue</th>
            </tr>
            {% for team in teams %}
                <tr>
                    <th>value team{{ loop.index }}</th>
                    <td>
                        {% if team.teamId==100 %}
                            {{ killsParticipantsTeam1 }}
                        {% else %}
                            {{ killsParticipantsTeam2 }}
                        {% endif %}
                    </td>
                    <td>
                        {% if team.teamId==100 %}
                            {{ totalMinionsKilledParticipantsTeam1 }}
                        {% else %}
                            {{ totalMinionsKilledParticipantsTeam2 }}
                        {% endif %}
                    </td>
                    <td>
                        {% if team.teamId==100 %}
                            {{ goldEarnedParticipantsTeam1 }}
                        {% else %}
                            {{ goldEarnedParticipantsTeam2 }}
                        {% endif %}
                    </td>
                    <td>
                        {% if team.firstBlood==false %}
                            0
                        {% else %}
                            {{ team.firstBlood }}
                        {% endif %}
                    </td>
                    <td>
                        {% if team.firstTower==false %}
                            0
                        {% else %}
                            {{ team.firstTower }}
                        {% endif %}
                    </td>
                    <td>
                        {% if team.firstInhibitor==false %}
                            0
                        {% else %}
                            {{ team.firstInhibitor }}
                        {% endif %}
                    </td>
                    <td>
                        {% if team.firstBaron==false %}
                            0
                        {% else %}
                            {{ team.firstBaron }}
                        {% endif %}
                    </td>
                    <td>
                        {% if team.firstDragon==false %}
                            0
                        {% else %}
                            {{ team.firstDragon }}
                        {% endif %}
                    </td>
                    <td>
                        {% if team.firstRiftHerald==false %}
                            0
                        {% else %}
                            {{ team.firstRiftHerald }}
                        {% endif %}
                    </td>
                    <td>{{ team.towerKills }}</td>
                    <td>{{ team.inhibitorKills }}</td>
                    <td>{{ team.baronKills }}</td>
                    <td>{{ team.dragonKills }}</td>
                    <td>
                        {% if team.win==\"Win\" %}
                            Win
                        {% else %}
                            Loose
                        {% endif %}
                    </td>
                </tr>

            {% endfor %}

        </table>
        <div class=\"row\" style=\"margin: 10px;\">

            {% for participant in participants %}
                {% if loop.index<=5 %}
                    <div class=\"card\" style=\"width: 18rem;margin-right: 15px;\">
                        <div class=\"card-header\" style=\"background-color: #34495e\">
                            Equipe 1
                        </div>
                        <ul class=\"list-group list-group-flush\">
                            {% for participantsIdentitie in participantsIdentities %}
                                {% if participant.participantId==participantsIdentitie.participantId %}
                                      <li class=\"list-group-item\">{{ participantsIdentitie.player.summonerName }}</li>
                                {% endif %}
                            {% endfor %}
                        </ul>
                    </div>
                {% endif %}
            {% endfor %}

        </div>
        <br>
        <div class=\"row\" style=\"margin-left: 10px;\">

            {% for participant in participants %}
                {% if loop.index>5 %}

                    <div class=\"card\" style=\"width: 18rem;margin-right: 15px;\">
                        <div class=\"card-header\" style=\"background-color: #c0392b\">
                            Equipe 2
                        </div>
                        <ul class=\"list-group list-group-flush\">
                            {% for participantsIdentitie in participantsIdentities %}
                                {% if participant.participantId==participantsIdentitie.participantId %}
                                    <li class=\"list-group-item\">{{ participantsIdentitie.player.summonerName }}</li>
                                {% endif %}
                            {% endfor %}
                        </ul>
                    </div>
                {% endif %}
            {% endfor %}
        </div>
    </div>

{% endblock %}", "detail_match.html.twig", "C:\\Users\\Aliaume\\projet-lol-predict\\templates\\detail_match.html.twig");
    }
}
