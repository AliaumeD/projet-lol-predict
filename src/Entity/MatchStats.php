<?php

namespace App\Entity;

use App\Repository\MatchStatsRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=MatchStatsRepository::class)
 */
class MatchStats
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="boolean",nullable=true,options={"default":"0"})
     */
    private $t1_first_blood;

    /**
     * @ORM\Column(type="integer",nullable=true,options={"default":"0"})
     */
    private $t2_first_blood;

    /**
     * @ORM\Column(type="boolean",nullable=true,options={"default":"0"})
     */
    private $t1_first_tower;

    /**
     * @ORM\Column(type="boolean",nullable=true,options={"default":"0"})
     */
    private $t2_first_tower;

    /**
     * @ORM\Column(type="boolean",nullable=true,options={"default":"0"})
     */
    private $t1_first_inhibitor;

    /**
     * @ORM\Column(type="boolean",nullable=true,options={"default":"0"})
     */
    private $t2_first_inhibitor;

    /**
     * @ORM\Column(type="boolean",nullable=true,options={"default":"0"})
     */
    private $t1_first_baron;

    /**
     * @ORM\Column(type="boolean",nullable=true,options={"default":"0"})
     */
    private $t2_first_baron;

    /**
     * @ORM\Column(type="boolean",nullable=true,options={"default":"0"})
     */
    private $t1_first_dragon;

    /**
     * @ORM\Column(type="boolean",nullable=true,options={"default":"0"})
     */
    private $t2_first_dragon;

    /**
     * @ORM\Column(type="boolean",nullable=true,options={"default":"0"})
     */
    private $t1_first_rift_herald;

    /**
     * @ORM\Column(type="boolean",nullable=true,options={"default":"0"})
     */
    private $t2_first_rift_herald;

    /**
     * @ORM\Column(type="integer",nullable=true,options={"default":"0"})
     */
    private $t1_tower;

    /**
     * @ORM\Column(type="integer",nullable=true,options={"default":"0"})
     */
    private $t2_tower;

    /**
     * @ORM\Column(type="integer",nullable=true,options={"default":"0"})
     */
    private $t1_inhibitor;

    /**
     * @ORM\Column(type="integer",nullable=true,options={"default":"0"})
     */
    private $t2_inhibitor;

    /**
     * @ORM\Column(type="integer",nullable=true,options={"default":"0"})
     */
    private $t1_baron;

    /**
     * @ORM\Column(type="integer",nullable=true,options={"default":"0"})
     */
    private $t2_baron;

    /**
     * @ORM\Column(type="integer",nullable=true,options={"default":"0"})
     */
    private $t1_dragon;

    /**
     * @ORM\Column(type="integer",nullable=true,options={"default":"0"})
     */
    private $t2_dragon;

    /**
     * @ORM\Column(type="integer",nullable=true,options={"default":"0"})
     */
    private $t1_kills;

    /**
     * @ORM\Column(type="integer",nullable=true,options={"default":"0"})
     */
    private $t2_kills;

    /**
     * @ORM\Column(type="integer",nullable=true,options={"default":"0"})
     */
    private $t1_minions;

    /**
     * @ORM\Column(type="integer",nullable=true,options={"default":"0"})
     */
    private $t2_minions;

    /**
     * @ORM\Column(type="integer",nullable=true,options={"default":"0"})
     */
    private $t1_gold_earned;

    /**
     * @ORM\Column(type="integer",nullable=true,options={"default":"0"})
     */
    private $t2_gold_earned;

    /**
     * @ORM\Column(type="boolean",nullable=true,options={"default":"0"})
     */
    private $t1_win;

    /**
     * @ORM\Column(type="boolean",nullable=true,options={"default":"0"})
     */
    private $t2_win;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getT1FirstBlood(): ?int
    {
        return $this->t1_first_blood;
    }

    public function setT1FirstBlood(int $t1_first_blood): self
    {
        $this->t1_first_blood = $t1_first_blood;

        return $this;
    }

    public function getT2FirstBlood(): ?int
    {
        return $this->t2_first_blood;
    }

    public function setT2FirstBlood(?int $t2_first_blood): self
    {
        $this->t2_first_blood = $t2_first_blood;

        return $this;
    }

    public function getT1FirstTower(): ?int
    {
        return $this->t1_first_tower;
    }

    public function setT1FirstTower(int $t1_first_tower): self
    {
        $this->t1_first_tower = $t1_first_tower;

        return $this;
    }

    public function getT2FirstTower(): ?int
    {
        return $this->t2_first_tower;
    }

    public function setT2FirstTower(int $t2_first_tower): self
    {
        $this->t2_first_tower = $t2_first_tower;

        return $this;
    }

    public function getT1FirstInhibitor(): ?int
    {
        return $this->t1_first_inhibitor;
    }

    public function setT1FirstInhibitor(int $t1_first_inhibitor): self
    {
        $this->t1_first_inhibitor = $t1_first_inhibitor;

        return $this;
    }

    public function getT2FirstInhibitor(): ?int
    {
        return $this->t2_first_inhibitor;
    }

    public function setT2FirstInhibitor(int $t2_first_inhibitor): self
    {
        $this->t2_first_inhibitor = $t2_first_inhibitor;

        return $this;
    }

    public function getT1FirstBaron(): ?int
    {
        return $this->t1_first_baron;
    }

    public function setT1FirstBaron(int $t1_first_baron): self
    {
        $this->t1_first_baron = $t1_first_baron;

        return $this;
    }

    public function getT2FirstBaron(): ?bool
    {
        return $this->t2_first_baron;
    }

    public function setT2FirstBaron(bool $t2_first_baron): self
    {
        $this->t2_first_baron = $t2_first_baron;

        return $this;
    }

    public function getT1FirstDragon(): ?bool
    {
        return $this->t1_first_dragon;
    }

    public function setT1FirstDragon(bool $t1_first_dragon): self
    {
        $this->t1_first_dragon = $t1_first_dragon;

        return $this;
    }

    public function getT2FirstDragon(): ?bool
    {
        return $this->t2_first_dragon;
    }

    public function setT2FirstDragon(bool $t2_first_dragon): self
    {
        $this->t2_first_dragon = $t2_first_dragon;

        return $this;
    }

    public function getT1FirstRiftHerald(): ?bool
    {
        return $this->t1_first_rift_herald;
    }

    public function setT1FirstRiftHerald(bool $t1_first_rift_herald): self
    {
        $this->t1_first_rift_herald = $t1_first_rift_herald;

        return $this;
    }

    public function getT2FirstRiftHerald(): ?bool
    {
        return $this->t2_first_rift_herald;
    }

    public function setT2FirstRiftHerald(bool $t2_first_rift_herald): self
    {
        $this->t2_first_rift_herald = $t2_first_rift_herald;

        return $this;
    }

    public function getT1Tower(): ?int
    {
        return $this->t1_tower;
    }

    public function setT1Tower(int $t1_tower): self
    {
        $this->t1_tower = $t1_tower;

        return $this;
    }

    public function getT2Tower(): ?int
    {
        return $this->t2_tower;
    }

    public function setT2Tower(int $t2_tower): self
    {
        $this->t2_tower = $t2_tower;

        return $this;
    }

    public function getT1Inhibitor(): ?int
    {
        return $this->t1_inhibitor;
    }

    public function setT1Inhibitor(int $t1_inhibitor): self
    {
        $this->t1_inhibitor = $t1_inhibitor;

        return $this;
    }

    public function getT2Inhibitor(): ?int
    {
        return $this->t2_inhibitor;
    }

    public function setT2Inhibitor(int $t2_inhibitor): self
    {
        $this->t2_inhibitor = $t2_inhibitor;

        return $this;
    }

    public function getT1Baron(): ?int
    {
        return $this->t1_baron;
    }

    public function setT1Baron(int $t1_baron): self
    {
        $this->t1_baron = $t1_baron;

        return $this;
    }

    public function getT2Baron(): ?int
    {
        return $this->t2_baron;
    }

    public function setT2Baron(int $t2_baron): self
    {
        $this->t2_baron = $t2_baron;

        return $this;
    }

    public function getT1Dragon(): ?int
    {
        return $this->t1_dragon;
    }

    public function setT1Dragon(int $t1_dragon): self
    {
        $this->t1_dragon = $t1_dragon;

        return $this;
    }

    public function getT2Dragon(): ?int
    {
        return $this->t2_dragon;
    }

    public function setT2Dragon(int $t2_dragon): self
    {
        $this->t2_dragon = $t2_dragon;

        return $this;
    }

    public function getT1Kills(): ?int
    {
        return $this->t1_kills;
    }

    public function setT1Kills(int $t1_kills): self
    {
        $this->t1_kills = $t1_kills;

        return $this;
    }

    public function getT2Kills(): ?int
    {
        return $this->t2_kills;
    }

    public function setT2Kills(int $t2_kills): self
    {
        $this->t2_kills = $t2_kills;

        return $this;
    }

    public function getT1Minions(): ?int
    {
        return $this->t1_minions;
    }

    public function setT1Minions(int $t1_minions): self
    {
        $this->t1_minions = $t1_minions;

        return $this;
    }

    public function getT2Minions(): ?int
    {
        return $this->t2_minions;
    }

    public function setT2Minions(int $t2_minions): self
    {
        $this->t2_minions = $t2_minions;

        return $this;
    }

    public function getT1GoldEarned(): ?int
    {
        return $this->t1_gold_earned;
    }

    public function setT1GoldEarned(int $t1_gold_earned): self
    {
        $this->t1_gold_earned = $t1_gold_earned;

        return $this;
    }

    public function getT2GoldEarned(): ?int
    {
        return $this->t2_gold_earned;
    }

    public function setT2GoldEarned(int $t2_gold_earned): self
    {
        $this->t2_gold_earned = $t2_gold_earned;

        return $this;
    }

    public function getT1Win(): ?bool
    {
        return $this->t1_win;
    }

    public function setT1Win(bool $t1_win): self
    {
        $this->t1_win = $t1_win;

        return $this;
    }

    public function getT2Win(): ?bool
    {
        return $this->t2_win;
    }

    public function setT2Win(bool $t2_win): self
    {
        $this->t2_win = $t2_win;

        return $this;
    }
}
