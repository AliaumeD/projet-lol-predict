<?php


namespace App\Controller;

use App\Entity\MatchStats;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class detail_match extends AbstractController
{
    /**
     * @Route("/detail_match.php",name="detail_match")
     */
    public function detail_match()
    {
        $apikey="RGAPI-8a52c14e-dd22-4fa9-ab54-327dbf3e4f13";
        //on récupére le pseudo
        $pseudo = $_POST['pseudo'];
        $client = HttpClient::create();
        //On récupére l'id de l'invocateur grace au pseudo tapé par l'utilisateur
        $response = $client->request('GET', "https://euw1.api.riotgames.com/lol/summoner/v4/summoners/by-name/" . $pseudo . "?api_key=" . $apikey . "");
        $Data = $response->ToArray();
        $id = $Data['accountId'];
        $response2 = $client->request('GET', "https://euw1.api.riotgames.com/lol/match/v4/matchlists/by-account/" . $id . "?api_key=" . $apikey . "");
        $Data2 = $response2->ToArray();
        $gameId=$Data2['matches'][0]['gameId'];
        $response3 = $client->request('GET', "https://euw1.api.riotgames.com/lol/match/v4/matches/" . $gameId . "?api_key=" . $apikey . "");
        $Data3 = $response3->ToArray();
        $t1_first_dragon='';$t2_first_dragon='';$t1_first_baron='';$t2_first_baron='';
        $t1_first_rift_herald='';$t2_first_rift_herald='';$t1_first_inhibitor='';$t2_first_inhibitor='';
        $t1_first_tower='';$t2_first_tower='';$t1_first_blood='';$t2_first_blood='';
        $t1_tower='';$t2_tower='';$t1_inhibitor='';$t2_inhibitor='';
        $t1_dragon='';$t2_dragon='';$t1_baron='';$t2_baron='';$t1_win='';$t2_win='';
        $killsParticipantsTeam1=0;
        $killsParticipantsTeam2=0;
        $goldEarnedParticipantsTeam1=0;
        $goldEarnedParticipantsTeam2=0;
        $totalMinionsKilledParticipantsTeam1=0;
        $totalMinionsKilledParticipantsTeam2=0;
        $i = 1;
        $entityManager = $this->getDoctrine()->getManager();
        foreach ($Data2['matches'] as $match) {
            if($i>50)
            {
                break;
            }
            $match_stats = new MatchStats();
            $gameId_bis = $match['gameId'];
            $response3_bis = $client->request('GET', "https://euw1.api.riotgames.com/lol/match/v4/matches/" . $gameId_bis . "?api_key=" . $apikey . "");
            $Data3_bis = $response3_bis->ToArray();
            foreach ($Data3_bis['teams'] as $listeTeams) {
                $teams[] = $listeTeams;
                if ($listeTeams['teamId'] == 100) {
                    if($listeTeams['win']=="Win")
                    {
                        $t1_win = 1;
                    }
                    else
                    {
                        $t1_win = 0;
                    }
                    $t1_first_dragon = $listeTeams['firstDragon'];
                    $t1_first_baron = $listeTeams['firstBaron'];
                    $t1_first_rift_herald = $listeTeams['firstRiftHerald'];
                    $t1_first_inhibitor = $listeTeams['firstInhibitor'];
                    $t1_first_tower = $listeTeams['firstTower'];
                    $t1_first_blood = $listeTeams['firstBlood'];
                    $t1_tower = $listeTeams['towerKills'];
                    $t1_inhibitor = $listeTeams['inhibitorKills'];
                    $t1_baron = $listeTeams['baronKills'];
                    $t1_dragon = $listeTeams['dragonKills'];

                }
                if ($listeTeams['teamId'] == 200) {
                    if($listeTeams['win']=="Win")
                    {
                        $t2_win = 1;
                    }
                    else
                    {
                        $t2_win = 0;
                    }
                    $t2_first_dragon = $listeTeams['firstDragon'];
                    $t2_first_baron = $listeTeams['firstBaron'];
                    $t2_first_rift_herald = $listeTeams['firstRiftHerald'];
                    $t2_first_inhibitor = $listeTeams['firstInhibitor'];
                    $t2_first_tower = $listeTeams['firstTower'];
                    $t2_first_blood = $listeTeams['firstBlood'];
                    $t2_tower = $listeTeams['towerKills'];
                    $t2_inhibitor = $listeTeams['inhibitorKills'];
                    $t2_baron = $listeTeams['baronKills'];
                    $t2_dragon = $listeTeams['dragonKills'];
                }
            }
            foreach ($Data3_bis['participants'] as $listeParticipants)
            {
                $participants[]=$listeParticipants;
                $statsParticipants[]=$listeParticipants['stats'];
                if($listeParticipants['participantId']>5)
                {
                    $killsParticipantsTeam1 += $listeParticipants['stats']['kills'];
                    $goldEarnedParticipantsTeam1 += $listeParticipants['stats']['goldEarned'];
                    $totalMinionsKilledParticipantsTeam1 += $listeParticipants['stats']['totalMinionsKilled'];
                }
                else {
                    $killsParticipantsTeam2 += $listeParticipants['stats']['kills'];
                    $goldEarnedParticipantsTeam2 += $listeParticipants['stats']['goldEarned'];
                    $totalMinionsKilledParticipantsTeam2 += $listeParticipants['stats']['totalMinionsKilled'];
                }
            }

            $match_stats->setT1FirstBlood($t1_first_blood);
            $match_stats->setT1FirstTower($t1_first_tower);
            $match_stats->setT1FirstInhibitor($t1_first_inhibitor);
            $match_stats->setT1FirstBaron($t1_first_baron);
            $match_stats->setT1FirstDragon($t1_first_dragon);
            $match_stats->setT1FirstRiftHerald($t1_first_rift_herald);
            $match_stats->setT1Tower($t1_tower);
            $match_stats->setT1Inhibitor($t1_inhibitor);
            $match_stats->setT1Baron($t1_baron);
            $match_stats->setT1Dragon($t1_dragon);
            $match_stats->setT2FirstBlood($t2_first_blood);
            $match_stats->setT2FirstTower($t2_first_tower);
            $match_stats->setT2FirstInhibitor($t2_first_inhibitor);
            $match_stats->setT2FirstBaron($t2_first_baron);
            $match_stats->setT2FirstDragon($t2_first_dragon);
            $match_stats->setT2FirstRiftHerald($t2_first_rift_herald);
            $match_stats->setT2Tower($t2_tower);
            $match_stats->setT2Inhibitor($t2_inhibitor);
            $match_stats->setT2Baron($t2_baron);
            $match_stats->setT2Dragon($t2_dragon);
            $match_stats->setT1Kills($killsParticipantsTeam1);
            $match_stats->setT1Minions($totalMinionsKilledParticipantsTeam1);
            $match_stats->setT1GoldEarned($goldEarnedParticipantsTeam1);
            $match_stats->setT2Kills($killsParticipantsTeam2);
            $match_stats->setT2Minions($totalMinionsKilledParticipantsTeam2);
            $match_stats->setT2GoldEarned($goldEarnedParticipantsTeam2);
            $match_stats->setT1Win($t1_win);
            $match_stats->setT2Win($t2_win);

            $entityManager->persist($match_stats);
            $entityManager->flush();
            $killsParticipantsTeam1=0;
            $killsParticipantsTeam2=0;
            $totalMinionsKilledParticipantsTeam1=0;
            $totalMinionsKilledParticipantsTeam2=0;
            $goldEarnedParticipantsTeam1=0;
            $goldEarnedParticipantsTeam2=0;
            $i++;
        }

        foreach ($Data3['participantIdentities'] as $listeParticipantsIdentities)
        {
            $participantsIdentities[]=$listeParticipantsIdentities;
        }

        return $this->render('detail_match.html.twig',
            ['id' => $id,
                'data' => $Data3,
                'teams' => $teams,
                'participants' => $participants,
                'statsParticipants' => $statsParticipants,
                'participantsIdentities' => $participantsIdentities,
                'killsParticipantsTeam1' => $killsParticipantsTeam1,
                'killsParticipantsTeam2' => $killsParticipantsTeam2,
                'goldEarnedParticipantsTeam1' => $goldEarnedParticipantsTeam1,
                'goldEarnedParticipantsTeam2' => $goldEarnedParticipantsTeam2,
                'totalMinionsKilledParticipantsTeam1' => $totalMinionsKilledParticipantsTeam1,
                'totalMinionsKilledParticipantsTeam2' => $totalMinionsKilledParticipantsTeam2,
            ]);
    }



}