<?php


namespace App\Controller;
use App\Entity\MatchStats;
use App\Entity\MatchStatsTime;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class recherche_match_time extends AbstractController
{

    /**
     * @Route("/recherche_match_time",name="recherche_match_time")
     */
    public function recherche_match_time()
    {
        $apikey="RGAPI-5de54e51-7345-4fc1-94b4-fbc018ff9324";
        //on récupére le pseudo
        $pseudo = $_POST['pseudo'];
        $client = HttpClient::create();
        //On récupére l'id de l'invocateur grace au pseudo tapé par l'utilisateur
        $response = $client->request('GET', "https://euw1.api.riotgames.com/lol/summoner/v4/summoners/by-name/" . $pseudo . "?api_key=" . $apikey . "");
        $Data = $response->ToArray();
        $id = $Data['accountId'];
        $response2 = $client->request('GET', "https://euw1.api.riotgames.com/lol/match/v4/matchlists/by-account/" . $id . "?api_key=" . $apikey . "");
        $Data2 = $response2->ToArray();
        $gameId=$Data2['matches'][0]['gameId'];
        $response3 = $client->request('GET', "https://euw1.api.riotgames.com/lol/match/v4/timelines/by-match/" . $gameId . "?api_key=" . $apikey . "");
        $Data3 = $response3->ToArray();
        $total_gold_10_t1=0;$total_gold_10_t2=0;$total_gold_15_t1=0;$total_gold_15_t2=0;$total_gold_20_t1=0;$total_gold_20_t2=0;$total_gold_25_t1=0;$total_gold_25_t2=0;
        $total_xp_10_t1=0;$total_xp_10_t2=0;$total_xp_15_t1=0;$total_xp_15_t2=0;$total_xp_20_t1=0;$total_xp_20_t2=0;$total_xp_25_t1=0;$total_xp_25_t2=0;
        $total_minions_10_t1=0;$total_minions_10_t2=0;$total_minions_15_t1=0;$total_minions_15_t2=0;$total_minions_20_t1=0;$total_minions_20_t2=0;$total_minions_25_t1=0;$total_minions_25_t2=0;
        $t1_win=0;$t2_win=0;
        $nb_frame=0;
        $i=0;
        $entityManager = $this->getDoctrine()->getManager();

        foreach($Data2['matches'] as $match) {
            if($i>40)
            {
                break;
            }
            $match_stats_time = new MatchStatsTime();
            $gameId_bis = $match['gameId'];
            $response3_bis = $client->request('GET', "https://euw1.api.riotgames.com/lol/match/v4/matches/" . $gameId_bis . "?api_key=" . $apikey . "");
            $Data3_bis = $response3_bis->ToArray();
            foreach ($Data3_bis['teams'] as $listeTeams) {
                if($listeTeams['teamId'] == 100) {
                    if ($listeTeams['win'] == "Win") {
                        $t1_win = 1;
                    } else {
                        $t1_win = 0;
                    }
                }
                if($listeTeams['teamId'] == 200) {
                    if ($listeTeams['win'] == "Win") {
                        $t2_win = 1;
                    } else {
                        $t2_win = 0;
                    }
                }
            }
            $response4 = $client->request('GET', "https://euw1.api.riotgames.com/lol/match/v4/timelines/by-match/" . $gameId_bis . "?api_key=" . $apikey . "");
            $Data4 = $response4->ToArray();
            foreach ($Data4['frames'] as $frames){
                $nb_frame++;
                if($nb_frame==10) {
                    foreach ($frames['participantFrames'] as $participant) {
                        if ($participant['participantId'] < 6 && $participant['participantId'] > 0) {
                            $total_gold_10_t1 += $participant['totalGold'];
                            $total_minions_10_t1 += $participant['minionsKilled'] + $participant['jungleMinionsKilled'];
                            $total_xp_10_t1 += $participant['xp'];
                            $resultat = $total_xp_10_t1;
                        }else{
                            $total_gold_10_t2 += $participant['totalGold'];
                            $total_minions_10_t2 += $participant['minionsKilled'] + $participant['jungleMinionsKilled'];
                            $total_xp_10_t2 += $participant['xp'];
                        }
                    }
                }
                if($nb_frame==15) {
                    foreach ($frames['participantFrames'] as $participant) {
                        if ($participant['participantId'] < 6 && $participant['participantId'] > 0) {
                            $total_gold_15_t1 += $participant['totalGold'];
                            $total_minions_15_t1 += $participant['minionsKilled'] + $participant['jungleMinionsKilled'];
                            $total_xp_15_t1 += $participant['xp'];
                        }
                        else{
                            $total_gold_15_t2 += $participant['totalGold'];
                            $total_minions_15_t2 += $participant['minionsKilled'] + $participant['jungleMinionsKilled'];
                            $total_xp_15_t2 += $participant['xp'];
                        }
                    }
                }
                if($nb_frame==20) {
                    foreach ($frames['participantFrames'] as $participant) {
                        if ($participant['participantId'] < 6 && $participant['participantId'] > 0) {
                            $total_gold_20_t1 += $participant['totalGold'];
                            $total_minions_20_t1 += $participant['minionsKilled'] + $participant['jungleMinionsKilled'];
                            $total_xp_20_t1 += $participant['xp'];
                        }
                        else{
                            $total_gold_20_t2 += $participant['totalGold'];
                            $total_minions_20_t2 += $participant['minionsKilled'] + $participant['jungleMinionsKilled'];
                            $total_xp_20_t2 += $participant['xp'];
                        }
                    }
                }
                if($nb_frame==25) {
                    foreach ($frames['participantFrames'] as $participant) {
                        if ($participant['participantId'] < 6 && $participant['participantId'] > 0) {
                            $total_gold_25_t1 += $participant['totalGold'];
                            $total_minions_25_t1 += $participant['minionsKilled'] + $participant['jungleMinionsKilled'];
                            $total_xp_25_t1 += $participant['xp'];
                        }
                        else{
                            $total_gold_25_t2 += $participant['totalGold'];
                            $total_minions_25_t2 += $participant['minionsKilled'] + $participant['jungleMinionsKilled'];
                            $total_xp_25_t2 += $participant['xp'];
                        }
                    }
                }

            }
            $match_stats_time->setTotalGold10T1($total_gold_10_t1);
            $match_stats_time->setTotalGold15T1($total_gold_15_t1);
            $match_stats_time->setTotalGold20T1($total_gold_20_t1);
            $match_stats_time->setTotalGold25T1($total_gold_25_t1);
            $match_stats_time->setTotalGold10T2($total_gold_10_t2);
            $match_stats_time->setTotalGold15T2($total_gold_15_t2);
            $match_stats_time->setTotalGold20T2($total_gold_20_t2);
            $match_stats_time->setTotalGold25T2($total_gold_25_t2);
            $match_stats_time->setTotalMinions10T1($total_minions_10_t1);
            $match_stats_time->setTotalMinions15T1($total_minions_15_t1);
            $match_stats_time->setTotalMinions20T1($total_minions_20_t1);
            $match_stats_time->setTotalMinions25T1($total_minions_25_t1);
            $match_stats_time->setTotalMinions10T2($total_minions_10_t2);
            $match_stats_time->setTotalMinions15T2($total_minions_15_t2);
            $match_stats_time->setTotalMinions20T2($total_minions_20_t2);
            $match_stats_time->setTotalMinions25T2($total_minions_25_t2);
            $match_stats_time->setTotalXp10T1($total_xp_10_t1);
            $match_stats_time->setTotalXp15T1($total_xp_15_t1);
            $match_stats_time->setTotalXp20T1($total_xp_20_t1);
            $match_stats_time->setTotalXp25T1($total_xp_25_t1);
            $match_stats_time->setTotalXp10T2($total_xp_10_t2);
            $match_stats_time->setTotalXp15T2($total_xp_15_t2);
            $match_stats_time->setTotalXp20T2($total_xp_20_t2);
            $match_stats_time->setTotalXp25T2($total_xp_25_t2);
            $match_stats_time->setT1Win($t1_win);
            $match_stats_time->setT2Win($t2_win);

            $entityManager->persist($match_stats_time);
            $entityManager->flush();


            $nb_frame=0;
            $total_gold_10_t1=0;$total_gold_10_t2=0;$total_gold_15_t1=0;$total_gold_15_t2=0;$total_gold_20_t1=0;$total_gold_20_t2=0;$total_gold_25_t1=0;$total_gold_25_t2=0;
            $total_xp_10_t1=0;$total_xp_10_t2=0;$total_xp_15_t1=0;$total_xp_15_t2=0;$total_xp_20_t1=0;$total_xp_20_t2=0;$total_xp_25_t1=0;$total_xp_25_t2=0;
            $total_minions_10_t1=0;$total_minions_10_t2=0;$total_minions_15_t1=0;$total_minions_15_t2=0;$total_minions_20_t1=0;$total_minions_20_t2=0;$total_minions_25_t1=0;$total_minions_25_t2=0;
            $t1_win;$t2_win;
            $i++;
        }
        return $this->render('recherche_match_time.html.twig');
    }
}